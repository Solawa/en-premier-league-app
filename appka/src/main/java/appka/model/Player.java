package appka.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Player {
    @JsonProperty("id")
    public int id;
    @JsonProperty("web_name")
    public String name;
    @JsonProperty("team")
    public int teamId;
    @JsonProperty("element_type")
    public Position position;

}
