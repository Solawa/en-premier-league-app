package appka.model;

import lombok.Data;

import java.util.Date;

@Data
public class GameWeekData {
    Date gameFinish;
    int event;
}
