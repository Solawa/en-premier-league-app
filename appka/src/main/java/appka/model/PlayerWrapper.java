package appka.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PlayerWrapper {
    @JsonProperty("elements")
    public Player[] players;
}
