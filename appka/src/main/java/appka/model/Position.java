package appka.model;

public enum Position {
    NULL,
    GK,
    DF,
    MF,
    FW;
}
