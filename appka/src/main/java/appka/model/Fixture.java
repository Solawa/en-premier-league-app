package appka.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
public class Fixture {
    @JsonProperty("code")
    public int code;
    @JsonProperty("event")
    public int event;
    @JsonProperty("kickoff_time")
    public Date kickOffTime;

}
