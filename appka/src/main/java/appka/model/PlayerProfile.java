package appka.model;

import lombok.Data;

/**
 * Class storing player summary
 */
@Data
public class PlayerProfile {
    public int playerId;
    public String name;
    public Position position;
    public int round;
    public int points;
    public int goals;
    public int goals_conceded;
    public int assists;
    public float minutes;
    public float influence;
    public int clean_sheets;
    public int saves;
    public int yellow_cards;
    public int red_cards;
}
