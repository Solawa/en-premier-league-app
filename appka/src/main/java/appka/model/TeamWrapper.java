package appka.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TeamWrapper {
    @JsonProperty("teams")
    public Team[] teams;
}
