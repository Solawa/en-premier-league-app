package appka.model;

import lombok.Data;

/**
 * Class storing data for every player`s performance
 */
@Data
public class Stats {
    public int playerId;
    public int fixture;
    public int round;
    public int points;
    public int goals;
    public int goals_conceded;
    public int assists;
    public float minutes;
    public float influence;
    public float creativity;
    public float threat;
    public float ict_index;
    public int clean_sheets;
    public int saves;
    public int yellow_cards;
    public int red_cards;
}
