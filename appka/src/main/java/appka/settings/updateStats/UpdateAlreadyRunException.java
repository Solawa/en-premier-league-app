package appka.settings.updateStats;

public class UpdateAlreadyRunException extends Exception {

    public UpdateAlreadyRunException() {
        super("Updating is already running");
    }
}
