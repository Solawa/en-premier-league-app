package appka.settings.updateStats;

import appka.model.GameWeekData;
import appka.model.Stats;
import appka.model.StatsWrapper;
import appka.repositories.StatsRepository;
import appka.settings.StatsUpdateDeserializer;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Class, which manage timers, i.e. start timer, stop timer
 */
@Component
public class TimerManager {

    private Timer timer;
    private StatsRepository statsRepository;
    private List<GameWeekData> gameWeekDataList;
    private String page;
    private boolean timerRun;
    private int index;

    void setPage(String page) {
        this.page = page;
    }

    void setGameWeekDataList(List<GameWeekData> gameWeekData) {
        this.gameWeekDataList = gameWeekData;
    }

    void setStatsRepository(StatsRepository statsRepository) {
        this.statsRepository = statsRepository;
    }

    public boolean getTimerRun() {
        return timerRun;
    }

    public void setTimerRun(boolean run){
        timerRun = run;
    }

    public TimerManager(){
        this.index = 0;
    }


    /**
     * Function start timer schedules for dates from gameWeekDataList based on index
     */

    public void startTimer() {
        timerRun = true;
        timer = new Timer();
        timer.schedule(new UpdateData(), gameWeekDataList.get(index).getGameFinish());
    }

    /**
     * Function stops timer
     */
    public void stopTimer() {
        timerRun = false;
        timer.cancel();
    }


    /**
     * TimerTask extension which download data from page, save it to repository
     * and create schedule for next Game Week end
     */
    public class UpdateData extends TimerTask {
        /**
         * Function call page consumer, then set up next task's schedule and
         * increment index
         */
        @Override
        public void run() {
            consumePage(gameWeekDataList.get(index).getEvent());
            if(index < gameWeekDataList.size()) {
                ++index;
                timer.schedule(new UpdateData(), gameWeekDataList.get(index).getGameFinish());
            }
        }

        /**
         * Function consume data from page and save it to repository
         * @param round - allow for page template specification
         */
        private void consumePage(int round) {
            SimpleModule module = new SimpleModule().addDeserializer(StatsWrapper.class, new StatsUpdateDeserializer(round));
            ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                    .registerModule(module);

            RestTemplate rest = new RestTemplate();
            String response = rest.getForObject(String.format(page, round), String.class);

            try {
                assert response != null;
                StatsWrapper base = mapper.readValue(response, StatsWrapper.class);
                for (Stats stats : base.playerStats ) {
                    statsRepository.save(stats);
                }
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
    }


}
