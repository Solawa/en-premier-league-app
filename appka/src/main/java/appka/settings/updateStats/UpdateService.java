package appka.settings.updateStats;

import appka.model.GameWeekData;
import appka.repositories.FixtureRepository;
import appka.repositories.StatsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UpdateService {

    private String pageTemplate;
    private FixtureRepository fixtureRepository;
    private StatsRepository statsRepository;
    private TimerManager timerManager;


    /**
     * Function inject fields and create context for Time manager
     */
    @Autowired
    public UpdateService(FixtureRepository fixtureRepository,
                         StatsRepository statsRepository,
                         @Value("${pages.gameweek}") String pageTemplate,
                         TimerManager timerManager) {
        this.pageTemplate = pageTemplate;
        this.fixtureRepository = fixtureRepository;
        this.statsRepository = statsRepository;

        List<GameWeekData> upcomingGameWeek = fixtureRepository.getFinishRound().stream()
                .filter(fixture -> fixture.getGameFinish().after(new Date()))
                .collect(Collectors.toList());
        timerManager.setStatsRepository(statsRepository);
        timerManager.setGameWeekDataList(upcomingGameWeek);
        timerManager.setPage(pageTemplate);
        timerManager.setTimerRun(false);
        this.timerManager = timerManager;
    }


    /**
     * Function filter timestamps of finished game weeks, saving only unfinished
     * then create timeManagers settings, and finally start update's timer
     */
    public void updateStats() throws UpdateAlreadyRunException {

        timerManager.startTimer();
    }

    public void switchOnUpdate(){
        if (!timerManager.getTimerRun())
            timerManager.startTimer();
    }


    public void switchOffUpdate(){
        if (timerManager.getTimerRun())
            timerManager.stopTimer();
    }

    public boolean canUpdate(){
        return !timerManager.getTimerRun();
    }

}



