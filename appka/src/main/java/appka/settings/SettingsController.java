package appka.settings;

import appka.settings.updateStats.UpdateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SettingsController {

    @Autowired
    private LoadDataService loadDataService;
    @Autowired
    private UpdateService updateService;


    @GetMapping("/")
    String home() {
        return "home";
    }

    @GetMapping("/settings")
    String settings(Model model) {
        model.addAttribute("canUpdate", updateService.canUpdate());
        return "settings";
    }

    @GetMapping("/player-info")
    public String getPlayers(Model model){
        loadDataService.loadPlayersInfo();
        return "redirect:/settings";
    }

    @GetMapping("/team-info")
    public String getTeams() {
        loadDataService.loadTeams();
        return "redirect:/settings";
    }

    @GetMapping("/fixtures")
    public String getFixtures() {
        loadDataService.loadFixtures();
        return "redirect:/settings";
    }

    @GetMapping("/update-start")
    public String updateStats() {
        updateService.switchOnUpdate();
        return "redirect:/settings";
    }

    @GetMapping("/update-stop")
    public String updateStop() {
        updateService.switchOffUpdate();
        return "redirect:/settings";
    }


    @GetMapping("/start")
    public String start() {
        loadDataService.loadPlayersInfo();
        loadDataService.loadTeams();
        loadDataService.loadFixtures();
        loadDataService.loadStats();
        return "redirect:/settings";
    }

}
