package appka.settings;

import appka.model.Stats;
import appka.model.StatsWrapper;
import appka.repositories.StatsRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.springframework.web.client.RestTemplate;



/**
 * Class to update history stats
 * Needs 3 variables, static repository in order to all threads be able to save data
 * static pageTemplate - all gameWeek REST endpoints have te same template
 * round - template specification
 *
 */
public class UpdateHistoryGameWeeks implements Runnable {

    static StatsRepository statsRepository;
    static String pageTemplate;
    private int round;

    public UpdateHistoryGameWeeks(int round) {
        this.round = round;
    }

    @Override
    public void run() {
        SimpleModule module = new SimpleModule().addDeserializer(StatsWrapper.class, new StatsUpdateDeserializer(round));
        ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .registerModule(module);

        RestTemplate rest = new RestTemplate();
        String response = rest.getForObject(String.format(pageTemplate, round), String.class);
        try {
            StatsWrapper base = mapper.readValue(response, StatsWrapper.class);
            for (Stats stats : base.playerStats ) {
                statsRepository.save(stats);
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

}