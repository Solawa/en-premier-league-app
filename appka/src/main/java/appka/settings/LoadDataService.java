package appka.settings;

import appka.model.*;
import appka.repositories.FixtureRepository;
import appka.repositories.PlayerRepository;
import appka.repositories.StatsRepository;
import appka.repositories.TeamRepository;
import appka.user.ProfileService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
@Slf4j
public class LoadDataService {

    @Value("${pages.bootstrap}")
    private String bootstrapUrl;
    @Value("${pages.fixtures}")
    private String fixtureUrl;
    @Value("${pages.gameweek}")
    private String gameWeekUrl;
    @Autowired
    private PlayerRepository playerRepository;
    @Autowired
    private StatsRepository statsRepository;
    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private FixtureRepository fixtureRepository;
    @Autowired
    private ProfileService profileService;
    @Autowired
    private ObjectMapper mapper;
    @Autowired
    private RestTemplate rest;

    /**
     * Function loads from page and saves to repository
     */
    public void loadPlayersInfo() {
        try {
            String response = rest.getForObject(bootstrapUrl, String.class);
            PlayerWrapper playerWrapper = mapper.readValue(response, PlayerWrapper.class);
            for (Player data : playerWrapper.players) {
                playerRepository.saveOrUpdate(data);
            }
        } catch (JsonProcessingException e) {
            log.error("Processing Json Error", e);
        }
    }

    /**
     * Function loads from page and saves to repository
     */
    public void loadTeams() {
        try {
            String response = rest.getForObject(bootstrapUrl, String.class);
            TeamWrapper teamWrapper = mapper.readValue(response, TeamWrapper.class);
            for (Team data : teamWrapper.teams) {
                teamRepository.saveOrUpdate(data);
            }
        } catch (JsonProcessingException e) {
            log.error("Processing Json Error", e);
        }
    }

    /**
     * Function loads from page and saves to repository
     */
    public void loadFixtures() {
        try {
            String response = rest.getForObject(fixtureUrl, String.class);
            Fixture[] fixtures = mapper.readValue(response, Fixture[].class);
            for (Fixture fix : fixtures) {
                fixtureRepository.saveOrUpdate(fix);
            }
        } catch (JsonProcessingException e) {
            log.error("Processing Json Error", e);
        }
    }

    /**
     * Function create pool of threads so as to get data from REST api
     * To do so call UpdateHistoryGameWeeks in each thread
     */

    public void loadStats() {
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        UpdateHistoryGameWeeks.statsRepository = statsRepository;
        UpdateHistoryGameWeeks.pageTemplate = gameWeekUrl;
        fixtureRepository.getFinishRound().stream()
                .filter(fixture -> fixture.getGameFinish().before(new Date()))
                .mapToInt(GameWeekData::getEvent)
                .distinct()
                .forEach(event -> executorService.submit(new UpdateHistoryGameWeeks(event)));
    }
}
