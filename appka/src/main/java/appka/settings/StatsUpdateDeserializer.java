package appka.settings;

import appka.model.Stats;
import appka.model.StatsWrapper;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.ArrayList;

public class StatsUpdateDeserializer extends JsonDeserializer<StatsWrapper> {

    private int round;

    public StatsUpdateDeserializer(int i) {
        this.round = i;
    }

    /**
     * Function deserialize JSON to Stats wrapped in StatsWrapper
     * @return StatsBase wrapper which contain Stats objects
     */
    @Override
    public StatsWrapper deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        StatsWrapper stats = new StatsWrapper();
        stats.playerStats = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode objectNode = mapper.readTree(p);
        objectNode.get("elements").elements().forEachRemaining(node -> {
            try {
                stats.playerStats.add(mapJsonIntoObject(node));
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        });
        return stats;
    }

    /**
     * Function map JSON to Stats fields
     * @return return finished Stats object
     * @throws NullPointerException if there is no data
     */
    private Stats mapJsonIntoObject(JsonNode node) {
        Stats stats = new Stats();
        stats.setPlayerId(node.get("id").asInt());
        stats.setRound(this.round);
        try {
            stats.setFixture(node.get("explain").get(0).get("fixture").asInt());
            stats.setMinutes(node.get("stats").get("minutes").asInt());
            stats.setRed_cards(node.get("stats").get("red_cards").asInt());
            stats.setYellow_cards(node.get("stats").get("yellow_cards").asInt());
            stats.setSaves(node.get("stats").get("saves").asInt());
            stats.setAssists(node.get("stats").get("assists").asInt());
            stats.setClean_sheets(node.get("stats").get("clean_sheets").asInt());
            stats.setGoals(node.get("stats").get("goals_scored").asInt());
            stats.setGoals_conceded(node.get("stats").get("goals_conceded").asInt());
            stats.setInfluence(node.get("stats").get("influence").asInt());
            stats.setPoints(node.get("stats").get("total_points").asInt());
            stats.setCreativity(node.get("stats").get("creativity").asInt());
            stats.setThreat(node.get("stats").get("threat").asInt());
            stats.setIct_index(node.get("stats").get("ict_index").asInt());
        }catch (NullPointerException e){
                System.out.println(String.format("Player id:%d in round:%d have no data",
                        stats.getPlayerId(), stats.getRound()));
        }
        return stats;
    }
}
