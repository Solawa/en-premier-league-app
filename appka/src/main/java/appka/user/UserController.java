package appka.user;

import appka.model.Player;
import appka.model.Stats;
import appka.repositories.PlayerRepository;
import appka.repositories.StatsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Controller
public class UserController {

    @Autowired
    private ProfileService profileService;
    @Autowired
    private StatsRepository statsRepository;
    @Autowired
    private PlayerRepository playerRepository;


    @GetMapping("/players")
    public String showPlayers(Model model) {
        model.addAttribute("profiles", profileService.findAll());
        return "table";
    }

    @GetMapping("/players/{id}")
    public String springMVC(Model modelMap, @PathVariable("id") String id) {

        List<Stats> statsList = statsRepository.findByPlayerId(id);
        Player playerInfo = playerRepository.findById(Integer.parseInt(id));

        modelMap.addAttribute("playerData", statsList);
        modelMap.addAttribute("playerInfo", playerInfo);
        return "chart";
    }
}
