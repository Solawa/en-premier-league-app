package appka.user;

import appka.model.PlayerProfile;
import appka.model.Position;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class ProfileService {

    private JdbcTemplate jdbc;

    private String queryForSummary = "select players.name, players.pos, player_id,count(round), sum(POINTS)," +
            " sum(GOALS), sum(GOALS_CONCEDED), sum(ASSISTS), sum(minutes), sum(influence), sum(clean_sheets)," +
            " sum(saves), sum(yellow_cards), sum(red_cards) from stats" +
            " left join players on stats.player_id= players.id group by player_id";

    @Autowired
    ProfileService(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    private PlayerProfile mapRowToPlayer(ResultSet rs, int rowNum) throws SQLException {
        PlayerProfile profile = new PlayerProfile();
        profile.setPlayerId(rs.getInt("player_id"));
        profile.setName(rs.getString("name"));
        profile.setPosition(Position.valueOf(rs.getString("pos")));
        profile.setRound(rs.getInt("count(round)"));
        profile.setPoints(rs.getInt("sum(points)"));
        profile.setGoals(rs.getInt("sum(goals)"));
        profile.setGoals_conceded(rs.getInt("sum(goals_conceded)"));
        profile.setAssists(rs.getInt("sum(assists)"));
        profile.setMinutes(rs.getFloat("sum(minutes)"));
        profile.setInfluence(rs.getInt("sum(influence)"));
        profile.setClean_sheets(rs.getInt("sum(clean_sheets)"));
        profile.setSaves(rs.getInt("sum(saves)"));
        profile.setYellow_cards(rs.getInt("sum(yellow_cards)"));
        profile.setRed_cards(rs.getInt("sum(red_cards)"));
        return profile;
    }

    public Iterable<PlayerProfile> findAll() {
        return jdbc.query(queryForSummary, this::mapRowToPlayer);
    }


}
