package appka.repositories;


import appka.model.Stats;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class StatsRepository {

    @Autowired
    private JdbcTemplate jdbc;


    public Iterable<Stats> findAll() {
        return jdbc.query("select record_id, player_id, round, fixture, points, goals, goals_conceded, assists, minutes, influence, creativity, threat, ict_index, clean_sheets, saves, yellow_cards, red_cards from Stats",
                this::mapRowToPlayer);
    }

    public List<Stats> findByPlayerId(String id) {
        String getById = "select record_id, player_id, round, fixture, points, goals, goals_conceded, assists, minutes, influence, creativity, threat, ict_index, clean_sheets, saves, yellow_cards, red_cards from stats where player_id=%s order by round";
        return jdbc.query(String.format(getById, id), this::mapRowToPlayer);
    }

    private Stats findByCode(String id) {
        try {
            String getByCode = "select record_id, player_id, round, fixture, points, goals, goals_conceded, assists, minutes, influence, creativity, threat, ict_index, clean_sheets, saves, yellow_cards, red_cards from stats where record_id=%s";
            return jdbc.queryForObject(String.format(getByCode, id), this::mapRowToPlayer);
        }
        catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    private Stats mapRowToPlayer(ResultSet rs, int rowNum) throws SQLException {
        Stats player = new Stats();
        player.setPlayerId(rs.getInt("player_id"));
        player.setRound(rs.getInt("round"));
        player.setFixture(rs.getInt("fixture"));
        player.setPoints(rs.getInt("points"));
        player.setGoals(rs.getInt("goals"));
        player.setGoals_conceded(rs.getInt("goals_conceded"));
        player.setAssists(rs.getInt("assists"));
        player.setMinutes(rs.getFloat("minutes"));
        player.setInfluence(rs.getInt("influence"));
        player.setCreativity(rs.getInt("creativity"));
        player.setThreat(rs.getInt("threat"));
        player.setIct_index(rs.getInt("ict_index"));
        player.setClean_sheets(rs.getInt("clean_sheets"));
        player.setSaves(rs.getInt("saves"));
        player.setYellow_cards(rs.getInt("yellow_cards"));
        player.setRed_cards(rs.getInt("red_cards"));
        return player;
    }

    private String createId(Stats stats) {
        return String.format("%03d%02d", stats.getPlayerId(), stats.getRound());
    }

    public Stats save(Stats stats) {
        if(findByCode(createId(stats)) == null) {
            jdbc.update("insert into Stats (record_id, player_id, round, fixture, points, goals, goals_conceded, assists, minutes, influence, creativity, threat, ict_index, clean_sheets, saves, yellow_cards, red_cards)" +
                            " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                    createId(stats),
                    stats.getPlayerId(),
                    stats.getRound(),
                    stats.getFixture(),
                    stats.getPoints(),
                    stats.getGoals(),
                    stats.getGoals_conceded(),
                    stats.getAssists(),
                    stats.getMinutes(),
                    stats.getInfluence(),
                    stats.getCreativity(),
                    stats.getThreat(),
                    stats.getIct_index(),
                    stats.getClean_sheets(),
                    stats.getSaves(),
                    stats.getYellow_cards(),
                    stats.getRed_cards());
        }
        return stats;
    }

}
