package appka.repositories;

import appka.model.Fixture;
import appka.model.GameWeekData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class FixtureRepository {

    @Autowired
    private JdbcTemplate jdbc;

    public Iterable<Fixture> findAll() {
        return jdbc.query("select code, event, kickOffTime from Fixtures",
                this::mapRowToFixture);
    }

    private Fixture mapRowToFixture(ResultSet rs, int rowNum) throws SQLException {
        Fixture fixture = new Fixture();
        fixture.setCode(rs.getInt("code"));
        fixture.setEvent(rs.getInt("event"));
        fixture.setKickOffTime(rs.getTimestamp("kickOffTime"));
        return fixture;
    }

    public Fixture findById(int code) {
        try {
            return jdbc.queryForObject("select code, event, kickOffTime from fixtures where code=?",
                    this::mapRowToFixture, code);
        }
        /**
         * No data with given id
         */
        catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    public Fixture saveOrUpdate(Fixture fixture) {
        if(findById(fixture.code) == null) {
            jdbc.update("insert into Fixtures (code, event, kickOffTime) " +
                            "values (?, ?, ?)",
                    fixture.getCode(),
                    fixture.getEvent(),
                    fixture.getKickOffTime());
        }
        else
            update(fixture);
        return fixture;
    }

    public Fixture update(Fixture fixture) {
        jdbc.update("update fixtures set event=?, kickOffTime=? where code=?",
                fixture.getEvent(), fixture.getKickOffTime(), fixture.getCode());
        return fixture;
    }

    /**
     * Function return timestamps of games' ending
     * @return List of GameWeekData, consists of round, and timestamp of game ending
     */

    public List<GameWeekData> getFinishRound() {
        return jdbc.query("select distinct dateadd('hour', 2, kickofftime) as end, event from fixtures order by end",
                this::mapRowToDate);
    }

    private GameWeekData mapRowToDate(ResultSet rs, int rowNum) throws SQLException {
        GameWeekData game = new GameWeekData();
        game.setGameFinish(rs.getTimestamp("end"));
        game.setEvent(rs.getInt("event"));
        return game;
    }

    public JdbcTemplate getJdbc() {
        return jdbc;
    }
}
