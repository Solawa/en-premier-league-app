package appka.repositories;

import appka.model.Player;
import appka.model.Position;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class PlayerRepository {

    @Autowired
    private JdbcTemplate jdbc;

    public Iterable<Player> findAll() {
        return jdbc.query("select id, name, team_id, pos from Players",
                this::mapRowToPlayer);
    }

    public Player findById(int id) {
        try {
            return jdbc.queryForObject("select id, name, team_id, pos from Players where id=?",
                    this::mapRowToPlayer, id);
        }catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    public int getLength() {
        return jdbc.queryForObject("select count(*) from players", Integer.class);
    }

    private Player mapRowToPlayer(ResultSet rs, int rowNum) throws SQLException {
        Player player = new Player();
        player.setId(rs.getInt("id"));
        player.setName(rs.getString("name"));
        player.setTeamId(rs.getInt("team_id"));
        player.setPosition(Position.valueOf(rs.getString("pos")));
        return player;
    }

    public Player saveOrUpdate(Player player) {
        if(findById(player.id) == null) {
            jdbc.update("insert into Players (id, name, team_id, pos) " +
                            "values (?, ?, ?, ?)",
                    player.getId(),
                    player.getName(),
                    player.getTeamId(),
                    player.getPosition().toString());
        }
        else
            update(player);
        return player;
    }

    public Player update(Player player){
        String queryUpdate = "update players set name='%s', team_id=%d, POS='%s' where id=%d";
        jdbc.update(String.format(queryUpdate, player.getName().replaceAll("'", "''"), player.getTeamId(), player.getPosition().toString(), player.getId()));
        return player;
    }

    public JdbcTemplate getJdbc() {
        return jdbc;
    }

}
