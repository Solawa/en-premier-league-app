package appka.repositories;


import appka.model.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class TeamRepository {

    @Autowired
    private JdbcTemplate jdbc;

    public Iterable<Team> findAll() {
        return jdbc.query("select id, name, short_name from Teams",
                this::mapRowToTeam);
    }

    public Team findById(int id) {
        try {
            return jdbc.queryForObject("select id, name, short_name from Teams where id=?",
                    this::mapRowToTeam, id);
        }
        catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    private Team mapRowToTeam(ResultSet rs, int rowNum) throws SQLException {
        Team team = new Team();
        team.setId(rs.getInt("id"));
        team.setName(rs.getString("name"));
        team.setShort_name(rs.getString("short_name"));
        return team;
    }

    public Team saveOrUpdate(Team team) {
        if(findById(team.id) == null) {
            jdbc.update("insert into Teams (id, name, short_name) " +
                            "values (?, ?, ?)",
                    team.getId(),
                    team.getName(),
                    team.getShort_name());
        }
        else
            update(team);
        return team;
    }

    public Team update(Team team) {
        String queryUpdate = "update teams set name='%s', short_name='%s' where id=%d";
        jdbc.update(String.format(queryUpdate, team.getName(), team.getShort_name(), team.getId()));
        return team;
    }

}
