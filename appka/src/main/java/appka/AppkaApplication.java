package appka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@Configuration
@SpringBootApplication
public class AppkaApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppkaApplication.class, args);
	}

}
