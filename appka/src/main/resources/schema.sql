create table if not exists Stats (
 record_id varchar(5) not null,
 player_id int not null,
 fixture int not null,
 round int not null,
 points int not null,
 goals int not null,
 goals_conceded int not null,
 assists int not null,
 minutes float not null,
 influence int not null,
 creativity int not null,
 threat int not null,
 ict_index int not null,
 clean_sheets int not null,
 saves int not null,
 yellow_cards int not null,
 red_cards int not null
);

create table if not exists Teams (
 id int not null,
 name varchar(25) not null,
 short_name varchar(4) not null
 );

create table if not exists Players (
   id int not null,
   name varchar(25) not null,
   team_id int not null,
   pos varchar(3) not null
 );

 create table if not exists Fixtures (
    code int not null,
    event int not null,
    kickOffTime TIMESTAMP not null
  );
