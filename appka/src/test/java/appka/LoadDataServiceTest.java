package appka;

import appka.model.*;
import appka.repositories.FixtureRepository;
import appka.repositories.PlayerRepository;
import appka.repositories.TeamRepository;
import appka.settings.LoadDataService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;


public class LoadDataServiceTest {

    String json = "{\n" +
            "  \"elements\": [\n" +
            "    {\n" +
            "      \"element_type\": 2,\n" +
            "      \"id\": 1,\n" +
            "      \"web_name\": \"Mustafi\"\n" +
            "    }\n" +
            "  ]\n" +
            "}";
    @Mock
    RestTemplate restTemplate;
    @Mock
    ObjectMapper objectMapper;
    @Mock
    PlayerRepository playerRepository;
    @Mock
    TeamRepository teamRepository;
    @Mock
    FixtureRepository fixtureRepository;
    @InjectMocks
    LoadDataService loadDataService;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void loadPlayerInfoTest() throws JsonProcessingException {
        ReflectionTestUtils.setField(loadDataService, "bootstrapUrl", "");
        PlayerWrapper wrapper= new PlayerWrapper();
        wrapper.players = new Player[5];
        Mockito.when(restTemplate.getForObject(Mockito.anyString(), Mockito.any(Class.class))).thenReturn("");
        Mockito.when(objectMapper.readValue(Mockito.anyString(), Mockito.any(Class.class))).thenReturn(wrapper);
        loadDataService.loadPlayersInfo();
        Mockito.verify(playerRepository, Mockito.times(5)).saveOrUpdate(null);
    }

    @Test
    public void loadTeamsTest() throws JsonProcessingException {
        ReflectionTestUtils.setField(loadDataService, "bootstrapUrl", "");
        TeamWrapper wrapper= new TeamWrapper();
        wrapper.teams = new Team[5];
        Mockito.when(restTemplate.getForObject(Mockito.anyString(), Mockito.any(Class.class))).thenReturn("");
        Mockito.when(objectMapper.readValue(Mockito.anyString(), Mockito.any(Class.class))).thenReturn(wrapper);
        loadDataService.loadTeams();
        Mockito.verify(teamRepository, Mockito.times(5)).saveOrUpdate(null);
    }

    @Test
    public void loadFixturesTest() throws JsonProcessingException {
        ReflectionTestUtils.setField(loadDataService, "fixtureUrl", "");
        Fixture[] fixtures= new Fixture[5];
        Mockito.when(restTemplate.getForObject(Mockito.anyString(), Mockito.any(Class.class))).thenReturn("");
        Mockito.when(objectMapper.readValue(Mockito.anyString(), Mockito.any(Class.class))).thenReturn(fixtures);
        loadDataService.loadFixtures();
        Mockito.verify(fixtureRepository, Mockito.times(5)).saveOrUpdate(null);
    }

    @Test
    public void loadStatsTest() throws JsonProcessingException {
        ReflectionTestUtils.setField(loadDataService, "fixtureUrl", "");
        Fixture[] fixtures= new Fixture[5];
        Mockito.when(restTemplate.getForObject(Mockito.anyString(), Mockito.any(Class.class))).thenReturn("");
        Mockito.when(objectMapper.readValue(Mockito.anyString(), Mockito.any(Class.class))).thenReturn(fixtures);
        loadDataService.loadFixtures();
        Mockito.verify(fixtureRepository, Mockito.times(5)).saveOrUpdate(null);
    }
}
