package appka;

import appka.model.Stats;
import appka.repositories.StatsRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@RunWith(SpringRunner.class)
@JdbcTest
@ComponentScan
public class StatsRepositoryTest {

    @Autowired
    private StatsRepository statsRepository;

    @Test
    public void testCrudOperations() {

        //insert
        Stats stats1 = new Stats();
        stats1.setPlayerId(4);
        stats1.setRound(9);
        stats1.setClean_sheets(12);
        Stats stats2 = new Stats();
        stats2.setPlayerId(4);
        stats2.setRound(10);
        stats2.setClean_sheets(122);
        statsRepository.save(stats1);
        statsRepository.save(stats2);

        //read
        List<Stats> loadedStats1 = statsRepository.findByPlayerId("4");
        Assert.assertNotNull(loadedStats1);
        Assert.assertEquals(9, loadedStats1.get(0).getRound());
        Assert.assertEquals(10, loadedStats1.get(1).getRound());
        Assert.assertEquals(12, loadedStats1.get(0).getClean_sheets());
        Assert.assertEquals(122, loadedStats1.get(1).getClean_sheets());
        Assert.assertEquals(2, loadedStats1.size());


        //Add new stats
        Stats stats3 = new Stats();
        stats3.setPlayerId(5);
        stats3.setRound(9);
        stats3.setClean_sheets(2);
        Stats stats4 = new Stats();
        stats4.setPlayerId(5);
        stats4.setRound(10);
        stats4.setClean_sheets(22);
        statsRepository.save(stats3);
        statsRepository.save(stats4);

        //read second stats
        List<Stats> loadedStats2 = statsRepository.findByPlayerId("5");
        Assert.assertNotNull(loadedStats2);
        Assert.assertEquals(9, loadedStats2.get(0).getRound());
        Assert.assertEquals(10, loadedStats2.get(1).getRound());
        Assert.assertEquals(2, loadedStats2.get(0).getClean_sheets());
        Assert.assertEquals(22, loadedStats2.get(1).getClean_sheets());
        Assert.assertEquals(2, loadedStats2.size());

        //loading all
        Iterable<Stats> list = statsRepository.findAll();
        AtomicInteger size = new AtomicInteger();
        list.forEach(record -> size.getAndIncrement());
        Assert.assertEquals(4, size.get());
    }
}

