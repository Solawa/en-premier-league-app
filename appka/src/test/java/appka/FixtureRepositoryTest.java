package appka;

import appka.model.Fixture;
import appka.repositories.FixtureRepository;
import appka.model.GameWeekData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.jdbc.JdbcTestUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;


public class FixtureRepositoryTest {

    @Spy
    JdbcTemplate jdbc = new JdbcTemplate();
    @InjectMocks
    private FixtureRepository fixtureRepository;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCrudOperations() {
        jdbc = new JdbcTemplate();
        fixtureRepository = new FixtureRepository();

        Date date = new Date();
        //insert
        Fixture fixture = new Fixture();
        fixture.setCode(100);
        fixture.setKickOffTime(date);
        fixture.setEvent(21);
        fixtureRepository.saveOrUpdate(fixture);

        //read
        Fixture loadedFixture = fixtureRepository.findById(100);
        Assert.assertNotNull(loadedFixture);
        Assert.assertEquals(21, loadedFixture.getEvent());
        Assert.assertEquals(date, loadedFixture.getKickOffTime());
        Assert.assertEquals(100, loadedFixture.getCode());

        int c = JdbcTestUtils.countRowsInTable(fixtureRepository.getJdbc(), "FIXTURES");
        Assert.assertEquals(1, c);

        //new date
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR, 1);
        Date date1 = calendar.getTime();

        //updating
        Fixture toBeUpdated = new Fixture();
        toBeUpdated.setCode(100);
        toBeUpdated.setEvent(1);
        toBeUpdated.setKickOffTime(date1);
        fixtureRepository.saveOrUpdate(toBeUpdated);

        //read again
        Fixture loadedFixture_ = fixtureRepository.findById(100);
        Assert.assertEquals(100, loadedFixture_.getCode());
        Assert.assertEquals(1, loadedFixture_.getEvent());
        Assert.assertEquals(date1, loadedFixture_.getKickOffTime());

        //add new player
        Fixture fixture2 = new Fixture();
        calendar.setTime(date1);
        calendar.add(Calendar.MINUTE, 20);
        Date date2 = calendar.getTime();
        fixture2.setCode(51);
        fixture2.setEvent(7);
        fixture2.setKickOffTime(date2);
        fixtureRepository.saveOrUpdate(fixture2);

        //read another player
        Fixture loadedFixture2 = fixtureRepository.findById(51);
        Assert.assertEquals(51, loadedFixture2.getCode());
        Assert.assertEquals(7, loadedFixture2.getEvent());
        Assert.assertEquals(date2, loadedFixture2.getKickOffTime());

        //loading all
        Iterable<Fixture> list = fixtureRepository.findAll();
        AtomicInteger size = new AtomicInteger();
        list.forEach(record -> size.getAndIncrement());
        Assert.assertEquals(2,size.get());

        //load list of end gameround
        List<GameWeekData> gameweek = fixtureRepository.getFinishRound();
        calendar.setTime(date1);
        calendar.add(Calendar.HOUR,2);
        Assert.assertEquals(calendar.getTime(), gameweek.get(0).getGameFinish());
        calendar.setTime(date2);
        calendar.add(Calendar.HOUR,2);
        Assert.assertEquals(calendar.getTime(), gameweek.get(1).getGameFinish());
    }


}
