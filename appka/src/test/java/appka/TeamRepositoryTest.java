package appka;

import appka.model.Team;
import appka.repositories.TeamRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.atomic.AtomicInteger;

@RunWith(SpringRunner.class)
@JdbcTest
@ComponentScan
public class TeamRepositoryTest {

    @Autowired
    private TeamRepository teamRepository;

    @Test
    public void testCrudOperations() {

        //insert
        Team team = new Team();
        team.setId(1);
        team.setName("Liverpool");
        team.setShort_name("LIV");
        teamRepository.saveOrUpdate(team);

        //read
        Team loadedPlayer = teamRepository.findById(1);
        Assert.assertNotNull(loadedPlayer);
        Assert.assertEquals("Liverpool", loadedPlayer.getName());
        Assert.assertEquals(1, loadedPlayer.getId());
        Assert.assertEquals("LIV", loadedPlayer.getShort_name());

        //updating team
        Team toBeUpdated = new Team();
        toBeUpdated.setId(1);
        toBeUpdated.setName("Barcelona");
        toBeUpdated.setShort_name("FCB");
        teamRepository.saveOrUpdate(toBeUpdated);

        //read again
        Team loadedTeam_ = teamRepository.findById(1);
        Assert.assertEquals("Barcelona", loadedTeam_.getName());
        Assert.assertEquals(1, loadedTeam_.getId());
        Assert.assertEquals("FCB", loadedTeam_.getShort_name());

        //add new team
        Team team2 = new Team();
        team2.setId(2);
        team2.setName("Arsenal");
        team2.setShort_name("ARS");
        teamRepository.saveOrUpdate(team2);

        //read another team
        Team loadedPlayer3 = teamRepository.findById(2);
        Assert.assertEquals("Arsenal", loadedPlayer3.getName());
        Assert.assertEquals(2, loadedPlayer3.getId());
        Assert.assertEquals("ARS", loadedPlayer3.getShort_name());

        //loading all
        Iterable<Team> list = teamRepository.findAll();
        AtomicInteger size = new AtomicInteger();
        list.forEach(record -> size.getAndIncrement());
        Assert.assertEquals(2,size.get());
    }


}
