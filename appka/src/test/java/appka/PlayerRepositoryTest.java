package appka;

import appka.model.Player;
import appka.model.Position;
import appka.repositories.PlayerRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.jdbc.JdbcTestUtils;

import java.util.concurrent.atomic.AtomicInteger;

@RunWith(SpringRunner.class)
@JdbcTest
@ComponentScan
public class PlayerRepositoryTest {

    @Autowired
    private PlayerRepository playerRepository;

    @Test
    public void testCrudOperations() {
        JdbcTestUtils.deleteFromTables(playerRepository.getJdbc(), "PLAYERS");

        //insert
        Player player = new Player();
        player.setId(1);
        player.setName("Wojtek");
        player.setTeamId(3);
        player.setPosition(Position.DF);
        playerRepository.saveOrUpdate(player);

        //read
        Player loadedPlayer = playerRepository.findById(1);
        Assert.assertNotNull(loadedPlayer);
        Assert.assertEquals("Wojtek", loadedPlayer.getName());
        Assert.assertEquals(1, loadedPlayer.getId());
        Assert.assertEquals(3, loadedPlayer.getTeamId());
        Assert.assertSame(Position.DF, loadedPlayer.getPosition());

        int c = JdbcTestUtils.countRowsInTable(playerRepository.getJdbc(), "PLAYERS");
        Assert.assertEquals(1, c);

        //updating player
        Player toBeUpdated = new Player();
        toBeUpdated.setId(1);
        toBeUpdated.setName("Wojtek");
        toBeUpdated.setTeamId(10);
        toBeUpdated.setPosition(Position.FW);
        playerRepository.saveOrUpdate(toBeUpdated);

        //read again
        Player loadedPlayer2 = playerRepository.findById(1);
        Assert.assertEquals("Wojtek", loadedPlayer2.getName());
        Assert.assertEquals(1, loadedPlayer2.getId());
        Assert.assertEquals(10, loadedPlayer2.getTeamId());
        Assert.assertSame(Position.FW, loadedPlayer2.getPosition());

        //add new player
        Player player2 = new Player();
        player2.setId(2);
        player2.setName("Irek");
        player2.setTeamId(3);
        player2.setPosition(Position.GK);
        playerRepository.saveOrUpdate(player2);

        //read another player
        Player loadedPlayer3 = playerRepository.findById(2);
        Assert.assertEquals("Irek", loadedPlayer3.getName());
        Assert.assertEquals(2, loadedPlayer3.getId());
        Assert.assertEquals(3, loadedPlayer3.getTeamId());
        Assert.assertSame(Position.GK, loadedPlayer3.getPosition());

        //loading all
        Iterable<Player> list = playerRepository.findAll();
        AtomicInteger size = new AtomicInteger();
        list.forEach(record -> size.getAndIncrement());
        Assert.assertEquals(2,size.get());

        //test getLenght
        Assert.assertEquals(2, playerRepository.getLength());

    }


}
