package appka;

import appka.repositories.PlayerRepository;
import appka.repositories.StatsRepository;
import appka.settings.LoadDataService;
import appka.user.UserController;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class SettingsController {

    @Mock
    private LoadDataService loadDataService;
    @Mock
    private StatsRepository statsRepository;
    @Mock
    private PlayerRepository playerRepository;

    @InjectMocks
    private UserController controller;

    private MockMvc mockMvc;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void testHomePage() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("home"));
    }

    @Test
    public void testSettings() throws Exception {
        mockMvc.perform(get("settings"))
                .andExpect(status().isOk())
                .andExpect(view().name("settings"));

    }

}
