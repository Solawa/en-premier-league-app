package appka;

import appka.model.Player;
import appka.model.PlayerProfile;
import appka.model.Stats;
import appka.repositories.PlayerRepository;
import appka.repositories.StatsRepository;
import appka.user.ProfileService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class UserControllerTest {

    @Mock
    private ProfileService profileService;
    @Mock
    private StatsRepository statsRepository;
    @Mock
    private PlayerRepository playerRepository;

    @InjectMocks
    private appka.user.UserController controller;

    private MockMvc mockMvc;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void showPlayersTable() throws Exception {
        List<PlayerProfile> players = new ArrayList<>();
        Player player = new Player();
        players.add(new PlayerProfile());
        players.add(new PlayerProfile());

        when(profileService.findAll()).thenReturn(players);

        mockMvc.perform(get("/players"))
                .andExpect(status().isOk())
                .andExpect(view().name("table"))
                .andExpect(model().attribute("profiles", hasSize(2)));
    }

    @Test
    public void testChart() throws Exception {
        List<Stats> playerData = new ArrayList<>();
        Player playerInfo = new Player();

        playerData.add(new Stats());
        playerData.add(new Stats());
        String id = "00101";

        when(statsRepository.findByPlayerId(id)).thenReturn(playerData);
        when(playerRepository.findById(Integer.parseInt(id))).thenReturn(playerInfo);

        mockMvc.perform(get(String.format("/players/%s", id)))
                .andExpect(status().isOk())
                .andExpect(view().name("chart"))
                .andExpect(model().attribute("playerData", hasSize(2)))
                .andExpect(model().attribute("playerInfo", equalTo(playerInfo)));


    }

}
