# Premier League stats app

## Requirements
For building and running the application you need:
*  [JDK](https://www.oracle.com/technetwork/java/javase/downloads/jdk13-downloads-5672538.html)
*  [Maven](https://maven.apache.org/)

## Technologies
*  Java 1.8
*  Maven 4.0
*  Spring boot 2.2.1.RELEASE
*  Lombook
*  JDBC
*  H2
*  Thymeleaf
*  JUnit 5.2.0

## General info
The aim of the application is to provide simple tool to visualize stats of English Premier League players.


## Running app locally
To build the full project, use 
```
mvn spring-boot:run
```
at location of pom.xml file

## First use
1. When program finished compiling, open browser and write : "localhost:8080"
2. To download data click "Go to settings", then "Download all data" and if you want enable live update click "Start update stats"
3. Application is dowloading huge volume of data so program can slow down, wait max 30 second for finish
4. Go back to home page by clicking "Back"
5. To see table consists of players summary click "Show player\`s data"
6. Click on a record in order to see data chart of selected player